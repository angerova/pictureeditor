

import Foundation
import UIKit






public protocol StickerViewDelegate: class {
    
    func stickerView(
        _ stickerView: StickerView
    )
    
}

public class StickerView: UIView {
    
    var isFlip = false
    var scaleCount: CGFloat = 1
    var startSize: CGFloat = 0
    var myLabel = UILabel()
    var handlingView: HandlingView!
    var maxScale: CGFloat = 0
    public weak var delegate: StickerViewDelegate?
    let imageView: UIImageView = UIImageView()

    var parentView: UIView!
    var rotation: CGFloat = 0.0
    
    init(frame: CGRect, parentView: UIView, myImage: UIImage, handlingView: HandlingView, innerFrame: CGFloat) {
        super.init(frame: frame)
        imageView.frame = CGRect(x: innerFrame, y: innerFrame, width: frame.size.width - (innerFrame)*2, height: frame.size.height - (innerFrame)*2)
        imageView.image = myImage
        self.addSubview(imageView)
        myInit(frame: frame, parentView: parentView, handlingView: handlingView, innerFrame: innerFrame)

    }
    
    init(frame: CGRect, parentView: UIView, myLabel: UILabel, handlingView: HandlingView, innerFrame: CGFloat) {
        super.init(frame: frame)
        self.myLabel = myLabel
         myLabel.frame = CGRect(x: innerFrame, y: innerFrame, width: frame.size.width - (innerFrame)*2, height: frame.size.height - (innerFrame)*2)
        self.addSubview(myLabel)
        myInit(frame: frame, parentView: parentView, handlingView: handlingView, innerFrame: innerFrame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func myInit(frame: CGRect, parentView: UIView, handlingView: HandlingView, innerFrame: CGFloat){
        self.handlingView = handlingView
        self.backgroundColor = .clear
        self.parentView = parentView
//        self.myLabel = myLabel
        startSize = sqrt((frame.size.height)*(frame.size.height) + (frame.size.width)*(frame.size.width))/2
        
//        myLabel.frame = CGRect(x: innerFrame, y: innerFrame, width: frame.size.width - (innerFrame)*2, height: frame.size.height - (innerFrame)*2)
        let maxWidth = parentView.frame.width
        let maxHeight = (frame.size.height/frame.size.width)*maxWidth
        let maxSize = sqrt(maxWidth*maxWidth + maxHeight*maxHeight)/2
        maxScale = maxSize/startSize
//        self.addSubview(myLabel)
        self.parentView.addSubview(self)
        addRecognizers()
    }
    
    func addRecognizers(){
        self.isUserInteractionEnabled = true
        let moveImage = UIPanGestureRecognizer(target: self, action: #selector(self.moveImage))
        let bringMeToFront = UITapGestureRecognizer(target: self, action: #selector(self.bringMeToFront))
        self.addGestureRecognizer(moveImage)
        self.addGestureRecognizer(bringMeToFront)
    }
    
    @objc func bringMeToFront(_ recognizer: UITapGestureRecognizer) {
            delegate?.stickerView(self)
    }
    
    @objc func moveImage(_ recognizer: UIPanGestureRecognizer) {
            delegate?.stickerView(self)
        switch recognizer.state {
        case .began:
          handlingView.showHandlingViews(isHidden: true)
        case .changed:
            handlingView.showHandlingViews(isHidden: true)
            let translation = recognizer.translation(in: parentView)

            self.center = CGPoint(x:self.center.x + translation.x,
                                  y: self.center.y + translation.y)
            handlingView.center = CGPoint(x: handlingView.center.x + translation.x,
                                          y: handlingView.center.y + translation.y)
            recognizer.setTranslation(CGPoint.zero, in: parentView)

            handlingView.replaceHandlingViews()
        case .ended:
            handlingView.showHandlingViews(isHidden: false)
    default :
        break
        }

    }
    
}





