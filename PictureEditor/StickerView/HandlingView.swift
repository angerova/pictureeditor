

import Foundation
import UIKit


public protocol HandlingViewDelegate: class {
    
    func handlingView(
        _ managedView: StickerView
    )
    
}

public class HandlingView: UIView {
    
    public weak var delegate: HandlingViewDelegate?
    var isText = false
    var isFlip = false
    var previousAngle:CGFloat = 0
    var tr = CGAffineTransform.identity
    var scaleCount: CGFloat = 1
    var startSize: CGFloat = 0
    var stickerViewHeight: CGFloat = 300
    var innerFrame: CGFloat = 20
    var handlingViewsHeight: CGFloat = 30
    
    var managedView: StickerView!
    
    let removeView: UIView = UIView()
    let scaleRotationView: UIView = UIView()
    let flipView: UIView = UIView()
    
    var borderColor: CGColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
    var borderWidth: CGFloat = 1
    
    var parentView: UIView!
    var rotation: CGFloat = 0.0
    
    
    init(frame: CGRect, parentView: UIView) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.parentView = parentView
    
        startSize = (frame.size.height)*sqrt(2)/2
        createBorder()
        createHandlingViews()
        addRecognizers()
        parentView.addSubview(self)
        showHandlingViews(isHidden: true)
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func addRecognizers(){
        self.isUserInteractionEnabled = true
        let moveImage = UIPanGestureRecognizer(target: self, action: #selector(self.moveImage))
        let scaleRotation = UIPanGestureRecognizer(target: self, action: #selector(self.scaleRotation))
        let remove = UITapGestureRecognizer(target: self, action: #selector(self.remove))
        let flipImage = UITapGestureRecognizer(target: self, action: #selector(self.flipImage))
        let addText = UITapGestureRecognizer(target: self, action: #selector(self.addText))
        let scaleText = UIPinchGestureRecognizer(target: self, action: #selector(self.scale))
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(self.rotate))
        rotate.delegate = self
        scaleText.delegate = self
        flipView.addGestureRecognizer(flipImage)
        self.addGestureRecognizer(rotate)
        self.addGestureRecognizer(scaleText)
        self.addGestureRecognizer(moveImage)
        self.addGestureRecognizer(addText)
        scaleRotationView.addGestureRecognizer(scaleRotation)
        removeView.addGestureRecognizer(remove)
    
    }
    
    private func createBorder(){
        self.layer.masksToBounds = true
        self.layer.borderColor = borderColor
        self.layer.borderWidth = borderWidth
    }
    
    private func createHandlingViews(){
    
        flipView.frame = CGRect(x: (handlingViewsHeight/2), y: stickerViewHeight - (handlingViewsHeight/2), width: handlingViewsHeight, height: handlingViewsHeight)
        removeView.frame = CGRect(x: stickerViewHeight - ( handlingViewsHeight/2), y: ( handlingViewsHeight/2), width: handlingViewsHeight, height: handlingViewsHeight)
        scaleRotationView.frame = CGRect(x: stickerViewHeight - ( handlingViewsHeight/2), y: stickerViewHeight - ( handlingViewsHeight/2), width: handlingViewsHeight, height: handlingViewsHeight)
        flipView.backgroundColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
        removeView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        scaleRotationView.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        scaleRotationView.roundCorners()
        removeView.roundCorners()
        flipView.roundCorners()
        
        parentView.addSubview(flipView)
        parentView.addSubview(removeView)
        parentView.addSubview(scaleRotationView)
    
    
    }
    
    func addMe(managedView: StickerView){
        self.managedView = managedView
        self.center = CGPoint( x:managedView.center.x,
                              y: managedView.center.y )
        self.layer.borderWidth = 1
        self.isHidden = false
        showHandlingViews(isHidden: false)
        parentView.bringSubviewToFront(managedView)
         parentView.bringSubviewToFront(self)
        parentView.bringSubviewToFront(flipView)
        parentView.bringSubviewToFront(removeView)
        parentView.bringSubviewToFront(scaleRotationView)
        
        let transform = CGAffineTransform(scaleX: managedView.scaleCount, y: managedView.scaleCount).rotated(by: managedView.rotation)
        self.transform = transform
        rotation = managedView.rotation
        replaceHandlingViews()
    }
    
    func replaceHandlingViews(){
        flipView.center = self.newBottomLeft
        scaleRotationView.center = self.newBottomRight
        removeView.center = self.newTopRight
    }
    
    @objc func remove(_ recognizer: UITapGestureRecognizer) {
        managedView.removeFromSuperview()
        self.isHidden = true
        showHandlingViews(isHidden: true)
        
    }
    
    @objc func addText(_ recognizer: UITapGestureRecognizer) {
        
      
        if isText == true{
            delegate?.handlingView(managedView)
        }
        
        
    }
    
    
    @objc func flipImage(_ recognizer: UITapGestureRecognizer) {
        
        var scaleX = 1
        if managedView.isFlip == false{
            scaleX = -1
        }
        
        managedView.imageView.transform = CGAffineTransform(scaleX: CGFloat(scaleX), y: 1)
        managedView.isFlip = !managedView.isFlip
    }
    
    @objc func moveImage(_ recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
            case .began:
                showHandlingViews(isHidden: true)
            case .changed:
            
                let translation = recognizer.translation(in: parentView)
                
                self.center = CGPoint(x: self.center.x + translation.x,
                y: self.center.y + translation.y)
                
                managedView.center = CGPoint(x: managedView.center.x + translation.x,
                                      y: managedView.center.y + translation.y)
                
                recognizer.setTranslation(CGPoint.zero, in: parentView)
                
                replaceHandlingViews()
            case .ended:
                showHandlingViews(isHidden: false)
            default :
                break
            }
        
    }
    
    fileprivate var startRotationAngle: CGFloat = 0
    
    @objc func scaleRotation(_ gesture: UIPanGestureRecognizer) {
        let location = gesture.location(in: parentView)
        let gestureRotation = CGFloat(angle(from: location)) - startRotationAngle
        switch gesture.state {
        case .began:
            showHandlingViews(isHidden: true)
            startRotationAngle = angle(from: location)
        
        case .changed:
        
            let b = sqrt((self.center.x - location.x)*(self.center.x - location.x) + (self.center.y - location.y)*(self.center.y - location.y))
            
            let newScaleCount = b/managedView.startSize
            
             if newScaleCount < managedView.maxScale || isText == false{
                scaleCount = newScaleCount
             }
            
            let transform = CGAffineTransform(scaleX: scaleCount, y: scaleCount).rotated(by: rotation - gestureRotation.degreesToRadians)
            self.transform = transform
            managedView.transform = transform
          
      
        case .ended:
              managedView.scaleCount = scaleCount
              
            rotation -= gestureRotation.degreesToRadians
 
            managedView.rotation = rotation
            replaceHandlingViews()
            showHandlingViews(isHidden: false)
        default :
            break
        }
    }
    
    func showHandlingViews(isHidden: Bool){
        scaleRotationView.isHidden = isHidden
        removeView.isHidden = isHidden
        flipView.isHidden = isHidden
    
    }
    
    func angle(from location: CGPoint) -> CGFloat {
        let deltaY = location.y - self.center.y
        let deltaX = location.x - self.center.x
        let angle = atan2(deltaY, deltaX) * 180 / .pi
        return angle < 0 ? abs(angle) : 360 - angle
    }
    
    @objc func scale(_ recognizer: UIPinchGestureRecognizer) {
        switch recognizer.state {
        case .began:
            showHandlingViews(isHidden: true)
            
        case .changed:

            if managedView.frame.width < parentView.frame.width || recognizer.scale < 1 || isText == false{
                self.transform = self.transform.scaledBy(x: recognizer.scale, y: recognizer.scale)
                managedView.transform = managedView.transform.scaledBy(x: recognizer.scale, y: recognizer.scale)
            }
            
            recognizer.scale = 1
        case .ended:
             flipView.center = self.newBottomLeft
             let xhd = managedView.newBottomLeft.x - managedView.newTopLeft.x
              let yhd = managedView.newBottomLeft.y - managedView.newTopLeft.y
             let xwd = managedView.newBottomLeft.x - managedView.newBottomRight.x
             let ywd = managedView.newBottomLeft.y - managedView.newBottomRight.y
             let currentHeight = sqrt(xhd*xhd + yhd*yhd)
             let currentWidth = sqrt(xwd*xwd + ywd*ywd)
            let currentSize = sqrt(currentWidth*currentWidth + currentHeight*currentHeight)/2
            scaleCount = (currentSize)/managedView.startSize
            managedView.scaleCount = scaleCount
            replaceHandlingViews()
            showHandlingViews(isHidden: false)
        default :
            break
        }
    }
    
    @objc func rotate(_ recognizer: UIRotationGestureRecognizer) {
      
        switch recognizer.state {
        case .began:
            showHandlingViews(isHidden: true)
            
        case .changed:
            
            self.transform = self.transform.rotated(by: recognizer.rotation)
            rotation += recognizer.rotation

            managedView.transform = managedView.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
 
         
            
        case .ended:
      
      
            managedView.rotation = rotation
            replaceHandlingViews()
            showHandlingViews(isHidden: false)
        default :
            break
        }
        
        
    }
}

extension HandlingView: UIGestureRecognizerDelegate {

    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
