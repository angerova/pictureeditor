//
//  ViewController.swift
//  PictureEditor
//
//  Created by Алина Ангерова on 03/08/2019.
//  Copyright © 2019 Алина Ангерова. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIGestureRecognizerDelegate  {

    var currentLabel: UILabel!
   
    @IBAction func addText(_ sender: Any) {
        let newLabel = UILabel()
        newLabel.textAlignment = .center
        newLabel.text = "Add Text"
        newLabel.font = UIFont(name: "Times New Roman", size: 60)
        newLabel.numberOfLines = 0
        newLabel.lineBreakMode = .byTruncatingTail
        newLabel.baselineAdjustment = .alignBaselines
        newLabel.adjustsFontSizeToFitWidth = true
        newLabel.minimumScaleFactor = 0.1
        let textView = StickerView(frame: CGRect(x: view.bounds.width/2 - 200/2, y: view.bounds.height/2 - 100/2, width: 200, height: 100), parentView: view, myLabel: newLabel, handlingView: handlingTextView, innerFrame: 0)
        textView.delegate = self
        handlingTextView.addMe(managedView: textView)
        hideStickerHandling()
    }
    
    @IBAction func addImage(_ sender: Any) {
        let stickerView = StickerView(frame: CGRect(x: view.bounds.width/2 - 300/2, y: view.bounds.height/2 - 300/2, width: 300, height: 300), parentView: view, myImage: UIImage(named: "item21")!, handlingView: handlingStickerView, innerFrame: 20)
        stickerView.delegate = self
        handlingStickerView.addMe(managedView: stickerView)

        hideTextHandling()
    }
    
    var handlingStickerView: HandlingView!
    var handlingTextView: HandlingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 1, green: 0.8174809633, blue: 0.7925103857, alpha: 1)
         view.isUserInteractionEnabled = true
        
        handlingStickerView = HandlingView(frame: CGRect(x: view.bounds.width/2 - 300/2, y: view.bounds.height/2 - 300/2, width: 300, height: 300), parentView: view)
        handlingStickerView.isHidden = true
        
        handlingTextView = HandlingView(frame: CGRect(x: view.bounds.width/2 - 200/2, y: view.bounds.height/2 - 100/2, width: 200, height: 100), parentView: view)
        handlingTextView.isHidden = true
        handlingTextView.isText = true
        let hideHandlingView = UITapGestureRecognizer(target: self, action: #selector(self.hideHandlingView))
        self.view.addGestureRecognizer(hideHandlingView)
        handlingTextView.delegate = self
      
        handlingTextView.flipView.removeFromSuperview()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goTextEdit" {
            if let nextViewController = segue.destination as? AddTextViewController {
                nextViewController.myLabel = currentLabel
            }
        }
    }
    
    @objc func hideHandlingView(_ recognizer: UITapGestureRecognizer) {
        hideTextHandling()
        hideStickerHandling()
    }
    
    func hideTextHandling(){
        handlingTextView.isHidden = true
        handlingTextView.showHandlingViews(isHidden: true)
    }
    
    func hideStickerHandling(){
        handlingStickerView.isHidden = true
        handlingStickerView.showHandlingViews(isHidden: true)
    }
}


extension ViewController: StickerViewDelegate{
    
    func stickerView(_ stickerView: StickerView) {
        hideTextHandling()
        hideStickerHandling()
        stickerView.handlingView.addMe(managedView: stickerView)
    }
    
    
}


extension ViewController: HandlingViewDelegate{
    func handlingView(_ managedView: StickerView) {
        currentLabel = managedView.myLabel
        self.performSegue(withIdentifier: "goTextEdit", sender: nil)
        
    }
}
