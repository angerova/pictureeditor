//
//  AddTextViewController.swift
//  PictureEditor
//
//  Created by Алина Ангерова on 09/08/2019.
//  Copyright © 2019 Алина Ангерова. All rights reserved.
//

import UIKit

class AddTextViewController: UIViewController {

    
    @IBAction func ok(_ sender: Any) {
    
        textField.resignFirstResponder()
        myLabel.text = label.text
        myLabel.font = label.font
        myLabel.backgroundColor = label.backgroundColor
        myLabel.textColor = label.textColor
        myLabel.font = myLabel.font.withSize(80)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func close(_ sender: Any) {
        textField.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet var colorButtons: [UIButton]!
    
    @IBOutlet weak var bakcgroundButton: UIButton!
    
    @IBOutlet weak var label: UILabel!
    
    var myLabel: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var textFieldViewBottom: NSLayoutConstraint!
    
     var keyboardHeight: CGFloat = 0
    let fonts = ["TimesNewRomanPSMT","SnellRoundhand","Zapfino","BodoniSvtyTwoSCITCTT-Book"]
    
   
    @IBAction func addText(_ sender: Any) {
        label.text = textField.text
 
    }
    
    @IBAction func changeTextColor(_ sender: UIButton) {
        
        if label.backgroundColor == .clear{
           label.textColor = colorButtons[sender.tag].backgroundColor

        }else{
            label.backgroundColor = colorButtons[sender.tag].backgroundColor
            label.textColor = (label.backgroundColor == .white) ? .black : .white

        }
        
        
    }
    
    
    @IBAction func setLabelBackground(_ sender: Any) {
      
        if label.backgroundColor == .clear{
            label.backgroundColor = label.textColor
            label.textColor = (label.textColor == .white) ? .black : .white
        }else{
            label.textColor = label.backgroundColor
            label.backgroundColor = .clear
        }
//        let mutable = NSMutableAttributedString(string: label.text!)
//        var startIndex = label.text!.startIndex
//        while let range = label.text!.range(of: "\\S+", options: .regularExpression, range: startIndex..<label.text!.endIndex) {
//            mutable.addAttribute(.backgroundColor, value: UIColor.cyan, range: NSRange(range, in: label.text!))
//            startIndex = range.upperBound
//        }
//        label.attributedText = mutable
    }
    
    @IBAction func showKeyboard(_ sender: Any) {
          textField.becomeFirstResponder()
    }
    
    
    @IBAction func showFont(_ sender: Any) {
        textField.resignFirstResponder()
    }
    
    
    @IBAction func OK(_ sender: Any) {
        
    }
    
    @IBOutlet weak var textField: UITextField!
    @IBAction func removeText(_ sender: Any) {
        textField.text = ""
        label.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = myLabel.text
        label.font = myLabel.font
        label.backgroundColor = myLabel.backgroundColor
        label.textColor = myLabel.textColor
        textField.text = myLabel.text
        
        
        textField.becomeFirstResponder()
        pickerView.delegate = self
        pickerView.dataSource = self
        selectFont(fontName: label.font.fontName)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)

    }
    
    func selectFont(fontName: String){
        print(fontName)
        let index = fonts.index(of: fontName)
        pickerView.selectRow(index!, inComponent: 0, animated: false)
        
    }

    @objc func keyboardWillShow(notification: Notification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        keyboardHeight = keyboardRectangle.height
        textFieldViewBottom.constant = keyboardHeight
        
    }
    
}


extension AddTextViewController: UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return fonts.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView{
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: fonts[row], size: 20)
            pickerLabel?.textAlignment = .center
        }
        pickerLabel?.text = fonts[row]
        pickerLabel?.textColor = .black
     
        return pickerLabel!
    
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        label.font = UIFont(name: fonts[row], size: 60)
    }
}
